﻿//ESTADOS
//0: parado
//1: subir
//2: bajar
//3: nuevo estado que usaré en un futuro
// a ver donde estoy cambiando
// ALVARO
// CRISTOPHER
// PEDRO
//EDGAR
//JERALD
//SARA
//ISA
/*
		╔═════════════════════════╗
		║   Juan Lu / Soreociem   ║
		╚═════════════════════════╝
*/


var t=0;
var contador;

$(document).ready(function(){
	//Arrancamos el timer que nos hara actualizar todo cada medio segundo
	contador=setInterval(actualizar,150);  	
	
	$('.celda').data('estado', 0).data('imagen', 0).html("<img src='0.png'>");
		
	$('.celda').click(darAlTopo);

});


function actualizar(){
	//Actualizamos el reloj
	//actualizarReloj();
	
	//Miramos si con el random tiene que salir algun topo
	var topoQueSale=Math.floor(Math.random()*50);
	
	
	
	if( $('.celda').eq(topoQueSale-1).data('estado')==0) $('.celda').eq(topoQueSale-1).data('estado',1);
	
	actualizarTopos();
}

function darAlTopo(){

	$(this).css("background-color","red");
}


function actualizarTopos(){

	$('.celda').each(function() {
	
	//		var $celdaActual= $(this);
			
			if( $(this).data('estado')==1){
			
				switch($(this).data('imagen')){
				case 0: $(this).data('imagen',1).html("<img src='1.png'>");
						break;
				case 1: $(this).data('imagen',2).html("<img src='2.png'>");
						break;
				case 2: $(this).data('imagen',3).html("<img src='3.png'>");
						break;
				case 3: $(this).data('imagen',4).html("<img src='4.png'>");
						break;
				case 4: $(this).data('imagen',5).html("<img src='5.png'>");
						break;
				case 5: $(this).data('imagen',6).html("<img src='6.png'>");
						break;
				case 6: $(this).data('imagen',7).html("<img src='7.png'>");
						break;
				case 7: $(this).data('imagen',6).data('estado',2).html("<img src='6.png'>");
						break;				
			}								
			}else if( $(this).data('estado')==2){
				if ($(this).data('imagen')==1) $(this).data('imagen',0).html("<img src='0.png'>").data('estado',0);
				else if ($(this).data('imagen')==2) $(this).data('imagen',1).html("<img src='1.png'>");
				else if ($(this).data('imagen')==3) $(this).data('imagen',2).html("<img src='2.png'>");
				else if ($(this).data('imagen')==4) $(this).data('imagen',3).html("<img src='3.png'>");
				else if ($(this).data('imagen')==5) $(this).data('imagen',4).html("<img src='4.png'>");
				else if ($(this).data('imagen')==6) $(this).data('imagen',5).html("<img src='5.png'>");
				else if ($(this).data('imagen')==7) $(this).data('imagen',6).html("<img src='6.png'>");
			}
		});	
}

function actualizarReloj()
{
	t=t+1;
	$('#tiempo').html(formateaHora(t));
}


function formateaHora(num){
var h, m,s;

h=num/3600;
h=parseInt(h);
num = num % 3600;
m= num /60;
m=parseInt(m);
num = num % 60;
s=num;
s=parseInt(s);

var tiempo="";

if (h<10) tiempo=tiempo+"0";
tiempo=tiempo+h+":";

if (m<10) tiempo=tiempo+"0";
tiempo=tiempo+m+":";

if (s<10) tiempo=tiempo+"0";
tiempo=tiempo+s;

return tiempo;

}